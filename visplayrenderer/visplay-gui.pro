#-------------------------------------------------
#
# Project created by QtCreator 2018-03-17T14:40:43
#
#-------------------------------------------------

QT       += widgets opengl svg webkit webkitwidgets dbus

LIBS     += -lmpv -lqhttpengine

CONFIG   += no_keywords

TARGET = visplayrenderer
TEMPLATE = app

DEFINES +=

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        mpvwidget.cpp \
        visplay/Window.cpp \
        visplay/CommandLineParser.cpp \
        visplay/QtWidgets/DigitalClock.cpp \
        visplay/QtWidgets/ClockHeader.cpp \
        visplay/layouts/DefaultLayout.cpp \
        visplay/layouts/FullscreenMPV.cpp \
        visplay/layouts/WebSplit.cpp \
        visplay/layouts/BBWLobbyLayoutText.cpp \
        visplay/layouts/BBWLobbyLayoutVideo.cpp

HEADERS += \
        mpvwidget.h \
        visplay/Window.h \
        visplay/CommandLineParser.h \
        visplay/QtWidgets/DigitalClock.h \
        visplay/QtWidgets/ClockHeader.h \
        visplay/layouts/Identifiers.h \
        visplay/layouts/BaseLayout.h \
        visplay/layouts/DefaultLayout.h \
        visplay/layouts/FullscreenMPV.h \
        visplay/layouts/WebSplit.h \
        visplay/layouts/BBWLobbyLayoutText.h \
        visplay/layouts/BBWLobbyLayoutVideo.h



release:DESTDIR = release/bin
release:OBJECTS_DIR = release/.obj
release:MOC_DIR = release/.moc
release:RCC_DIR = release/.rcc
release:UI_DIR = release/.ui

debug:DESTDIR = release/bin
debug:OBJECTS_DIR = release/.obj
debug:MOC_DIR = release/.moc
debug:RCC_DIR = release/.rcc
debug:UI_DIR =debugd/release/.ui

unix {
        target.path = release/visplayrenderer/
        INSTALLS += target
}
