/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BBWLAYOUTVIDEO_H
#define BBWLAYOUTVIDEO_H

#include <QString>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>
#include <QFont>
#include <QPointer>
#include <QJsonObject>
#include <QTimer>

#include "mpvwidget.h"

#include "visplay/QtWidgets/ClockHeader.h"
#include "visplay/layouts/BaseLayout.h"

namespace visplay::layouts
{

class BBWLayoutVideo : public BaseLayout 
{
    Q_OBJECT

    public:
        BBWLayoutVideo(QJsonObject& obj);
        ~BBWLayoutVideo();

        void display();
        void connect_event_loop(QEventLoop* event_loop);
    private:
        int fontSize;
        QString headerText, path, fontType;
        MpvWidget                   *mpv_widget;
};

}

#endif