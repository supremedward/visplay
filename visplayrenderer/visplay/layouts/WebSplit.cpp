/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/WebSplit.h"

namespace visplay::layouts
{

WebSplit::WebSplit(QJsonObject& obj)
{
    layout = new QVBoxLayout;
    mpv_widget = new MpvWidget;
    web_view = new QWebView;

    path = obj["path"].toString();
    url = obj["url"].toString();

    qDebug() << "Creating WebSplit" << "\n" << "with path" << path << "and URL" << url;

    layout->addWidget(web_view);
    layout->addWidget(mpv_widget);
    layout->setContentsMargins(0, 0, 0, 0);
}

WebSplit::~WebSplit()
{

}

void WebSplit::display()
{
    mpv_widget->command(QStringList() << "loadfile" << path);
    web_view->setUrl(url);
}

void WebSplit::connect_event_loop(QEventLoop* event_loop)
{
    connect(this->mpv_widget, SIGNAL(playback_idle()), event_loop, SLOT(quit()));
}

} // end namespace visplay::layouts
