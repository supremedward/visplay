/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/FullscreenMPV.h"

namespace visplay::layouts
{

FullscreenMPV::FullscreenMPV(QJsonObject& obj)
{
    layout = new QVBoxLayout;

    mpv_widget = new MpvWidget();

    path = obj["path"].toString();
    qDebug() << "Creating FullscreenMPV" << "\n" << "with path" << path;

    layout->addWidget(mpv_widget);
    layout->setContentsMargins(0, 0, 0, 0);
}

FullscreenMPV::~FullscreenMPV()
{

}

void FullscreenMPV::display()
{
    mpv_widget->command(QStringList() << "loadfile" << path);
}

void FullscreenMPV::connect_event_loop(QEventLoop* event_loop)
{
    connect(this->mpv_widget, SIGNAL(playback_idle()), event_loop, SLOT(quit()));
}

} // end namespace visplay::layouts
