/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/DefaultLayout.h"

#include <QString>

namespace visplay::layouts
{

DefaultLayout::DefaultLayout(QString label)
{
    initalize(label);
}

DefaultLayout::DefaultLayout(QJsonObject& obj)
{
    initalize(obj["string"].toString());
}

DefaultLayout::~DefaultLayout()
{

}

void DefaultLayout::display()
{

}

void DefaultLayout::initalize(QString label)
{
    layout = new QVBoxLayout;
    center_label = new QLabel(label);
    center_label->setAlignment(Qt::AlignCenter);
    center_label->setMinimumSize( QSize(0,0) );
    center_label->setMaximumSize( QSize(16777215, 16777215) );
    center_label->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Preferred );
    QFont f( "Arial", 60, QFont::Bold);
    center_label->setFont(f);
    layout->addWidget(center_label);
    layout->setContentsMargins(0, 0, 0, 0);
}

void DefaultLayout::connect_event_loop(QEventLoop* event_loop)
{
    this->playback_timer = new QTimer(this);
    this->playback_timer->setSingleShot(true);

    connect(this->playback_timer, SIGNAL(timeout()), event_loop, SLOT(quit()));
    this->playback_timer->start(5000);
}

} // end namespace visplay::layouts
