/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef DEFAULTLAYOUT_H
#define DEFAULTLAYOUT_H

#include "mpvwidget.h"

#include "visplay/layouts/BaseLayout.h"

#include <QPointer>
#include <QObject>
#include <QLabel>
#include <QTimer>
#include <QJsonObject>


namespace visplay::layouts {

class DefaultLayout : public BaseLayout
{
    Q_OBJECT

    public:
        DefaultLayout(QString label);
        DefaultLayout(QJsonObject& obj);
        ~DefaultLayout();

        void display();
        void connect_event_loop(QEventLoop* event_loop);

    private:
        void initalize(QString label);

        QPointer<QLabel>        center_label;

        QTimer                  *playback_timer;
};

} // end namespace visplay::layouts
#endif // DEFAULTLAYOUT_H
