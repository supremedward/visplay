/*
 *   This file is part of Visplay.
 *
 *   Visplay is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Visplay is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Visplay.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "visplay/layouts/BBWLobbyLayoutVideo.h"

#include "visplay/QtWidgets/DigitalClock.h"

#include <QtWidgets>

namespace visplay::layouts 
{

BBWLayoutVideo::BBWLayoutVideo(QJsonObject& obj) 
{
    fontType = "Arial";
    fontSize = 24;
    QFont font(fontType, fontSize);

    layout = new QVBoxLayout();
    
    QPointer<ClockHeader> headerClock = new ClockHeader;
    headerClock->setFixedHeight(fontSize + 10 * (fontSize / 12));

    mpv_widget = new MpvWidget;

    headerText = obj["header"].toString();
    path = obj["path"].toString();

    headerClock->setHeaderText(headerText);

    layout->addWidget(headerClock);
    layout->addWidget(mpv_widget);
     
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    qDebug() << "Creating BBWLobbyLayout";
}

BBWLayoutVideo::~BBWLayoutVideo()
{

}

void BBWLayoutVideo::display() 
{
    mpv_widget->command(QStringList() << "loadfile" << path);
}

void BBWLayoutVideo::connect_event_loop(QEventLoop* event_loop)
{
    connect(this->mpv_widget, SIGNAL(playback_idle()), event_loop, SLOT(quit()));
}

}