<!--
Add a concise, descriptive title to this bug report.

Please fill out the information below. If it's not relevant to your issue, just
skip it. If you have anything else to add on, please do so.
-->

/label ~bug

## Brief Description of Bug

## Environment

- **OS:** <!-- Arch Linux/Fedora/Windows/... -->
- **Python Version:** <!-- Python 3.7 -->
- <!-- Any other pertinent information -->

## Steps to Reproduce

1. <!-- do this -->
2. <!-- then that -->

## Expected Result

<!-- it should do... -->

## Actual Result

<!-- it did... -->
