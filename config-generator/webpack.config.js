const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const sourceFolder = 'frontend';

module.exports = {
  entry: `./${sourceFolder}/index.js`,
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'index_bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `./${sourceFolder}/index.html`,
    }),
  ],
};
