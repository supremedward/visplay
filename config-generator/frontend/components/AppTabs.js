import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import AssetTab from './AssetTab';


const styles = theme => {
  return {
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
  };
};

/**
 * Main tab container
 */
class AppTabs extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  state = {
    tabValue: 0,

    // Assets
    assets: [],
    assetsLoading: true,

    // Playlists
    playlists: [],
    playlistsLoading: true,
  }

  refreshInterval = null

  updateAssetStateFromServer() {
    axios.get('/assets')
      .then(res =>
        this.setState({ assets: res.data, assetsLoading: false })
      );
  }

  componentDidMount() {
    this.setState({ assetsLoading: true, playlistsLoading: true });
    this.updateAssetStateFromServer();

    // Refetch the assets every 5 seconds
    this.refreshInterval = setInterval(this.updateAssetStateFromServer.bind(this), 5000);
  }

  on_TabChange(event, tabValue) {
    this.setState({ tabValue });
  }

  on_assets_change(action, value) {
    if (['add', 'delete', 'edit'].indexOf(action) < 0) {
      throw new Error(`Invalid asset change ${action}`);
    }

    axios.post(`/asset/${action}`, value).then(() => {
      switch (action) {
        case 'add':
          this.setState({ assets: this.state.assets.concat([value]) });
          break;
        case 'delete':
          // Filter out the one which is being deleted.
          this.setState({ assets: this.state.assets.filter(a => a.name !== value.asset_name) });
          break;
        case 'edit':
          this.setState({
            assets: this.state.assets.map(a =>
              a.name === value.asset_name ? value.new_value : a),
          });
          break;
        default:
          throw new Error(`Invalid asset change ${action}`);
      }
    }).catch(error => {
      throw new Error(`Failed to commit changes: ${error}`);
    });
  }

  render() {
    const { classes } = this.props;
    const { assets, assetsLoading, tabValue } = this.state;

    return (
      <div className={classes.root}>
        <Paper square>
          <Tabs value={tabValue} centered
            onChange={this.on_TabChange.bind(this)}>
            <Tab label="Assets" />
            <Tab label="Playlists" />
          </Tabs>
        </Paper>
        {
          tabValue === 0 && <AssetTab
            assets={assets}
            on_assets_change={this.on_assets_change.bind(this)}
            loading={assetsLoading} />
        }
        {tabValue === 1 && <div>Not done...</div>}
      </div>
    );
  }
}

export default withStyles(styles)(AppTabs);
