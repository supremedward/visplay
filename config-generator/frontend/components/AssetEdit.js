import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import PropTypes from 'prop-types';

/**
 * A list of all the assets.
 *
 * @class
 */
class AssetEdit extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    is_editing: PropTypes.bool.isRequired,
    initial_edit_state: PropTypes.object,
    open: PropTypes.bool.isRequired,
    on_close: PropTypes.func.isRequired,
    on_add_edit: PropTypes.func.isRequired,
  };

  state = { name: '', uri: '' }

  on_field_change = name => event => {
    this.setState({ [name]: event.target.value });
  }

  render() {
    const { classes, open, on_close, on_add_edit, is_editing, initial_edit_state } = this.props;

    const clean_close = (...rest) => {
      this.setState({ name: '', uri: '' });
      on_close.call(on_close, ...rest);
    };

    const on_addEditButton_click = () => {
      on_add_edit(this.state);
      this.setState({ name: '', uri: '' });
    };

    const onEntering = () => {
      if (is_editing) {
        this.setState(initial_edit_state);
      }
    };

    return (
      <Dialog
        open={open}
        onClose={clean_close}
        onEntering={onEntering}
        maxWidth="md"
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{is_editing ? 'Edit' : 'Add'} Asset</DialogTitle>
        <DialogContent>
          <TextField
            required
            id="name"
            label="Asset Name"
            value={this.state.name}
            onChange={this.on_field_change('name')}
            className={classNames(classes.textField, classes.nameField)}
            margin="normal"
          />
          <TextField
            required
            id="uri"
            label="Asset URI"
            value={this.state.uri}
            onChange={this.on_field_change('uri')}
            className={classNames(classes.textField, classes.uriField)}
            margin="normal"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={clean_close} color="primary">
            Cancel
          </Button>
          <Button onClick={on_addEditButton_click} color="primary">
            {is_editing ? 'Edit' : 'Add'}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(theme => {
  return {
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    nameField: { width: 200 },
    uriField: { width: 400 },
  };
})(AssetEdit);
