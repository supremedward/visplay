#! /usr/bin/env python3.7

import argparse

import controllers  # noqa: F401
from bottle import run

# Parse arguments
parser = argparse.ArgumentParser(
    description='Vislay Configuration Generator backend server.')
parser.add_argument('port', nargs='?', default=8080, type=int)
parser.add_argument('-d', '--debug', action='store_true')

args = parser.parse_args()

if __name__ == '__main__':
    run(host='0.0.0.0', port=args.port, debug=args.debug)
