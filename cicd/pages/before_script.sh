#!/usr/bin/env bash

set -xe

# Install Requirements
pip3 install -r requirements.txt
pip3 install .
